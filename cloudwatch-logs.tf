resource "aws_cloudwatch_log_group" "ecs" {
  name              = "/aws/ecs/${var.app}"
  retention_in_days = "180"
  tags              = merge(local.common-tags,
    {
      Name = "/aws/ecs/${var.app}"
    }
  )
}

resource "aws_cloudwatch_log_metric_filter" "ecs-404s" {
  name           = "${var.app}-alarm"
  pattern        = " 404 "
  log_group_name = aws_cloudwatch_log_group.ecs.name
  metric_transformation {
    name      = local.cw-ecs-404-metric-name
    namespace = local.cw-ecs-namespace
    value     = "1"
  }
}

resource "aws_cloudwatch_log_group" "codebuild" {
  name              = "/aws/codebuild/${var.app}"
  retention_in_days = "180"
  tags              = merge(local.common-tags,
    {
      Name = "/aws/codebuild/${var.app}"
    }
  )
}

resource "aws_cloudwatch_log_group" "codebuild-test" {
  name              = "/aws/codebuild/${var.app}-test"
  retention_in_days = "180"
  tags              = merge(local.common-tags,
    {
      Name = "/aws/codebuild/${var.app}-test"
    }
  )
}

resource "aws_cloudwatch_log_group" "deploy-hook" {
  name              = "/aws/lambda/${var.app}-deploy-hook"
  retention_in_days = "180"
  tags              = merge(local.common-tags,
    {
      Name = "/aws/lambda/${var.app}-deploy-hook"
    }
  )
}
