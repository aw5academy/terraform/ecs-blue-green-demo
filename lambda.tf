data "archive_file" "deploy-hook" {
  output_path = ".lambda/deploy-hook.zip"
  source_dir  = "lambda-src/deploy-hook"
  type        = "zip"
}

resource "aws_lambda_function" "deploy-hook" {
  environment {
    variables = {
      BUCKET_NAME = aws_s3_bucket.codepipeline-artifacts.id
    }
  }
  filename         = ".lambda/deploy-hook.zip"
  function_name    = "${var.app}-deploy-hook"
  handler          = "lambda_function.lambda_handler"
  role             = aws_iam_role.deploy-hook.arn
  runtime          = "python3.8"
  source_code_hash = data.archive_file.deploy-hook.output_base64sha256
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-deploy-hook"
    }
  )
  timeout          = "30"
}
