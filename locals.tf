locals {
  common-tags = {
    App = var.app
  }
  cw-ecs-namespace       = "${var.app}/ECS"
  cw-ecs-404-metric-name = "404s"
}
