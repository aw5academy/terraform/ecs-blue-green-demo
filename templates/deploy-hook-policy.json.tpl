{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": [
        "arn:aws:logs:*:${aws_account_id}:log-group:/aws/lambda/${function-name}",
        "arn:aws:logs:*:${aws_account_id}:log-group:/aws/lambda/${function-name}:log-stream:*"
      ]
    },
    {
      "Effect": "Allow",
      "Action": [
        "codedeploy:GetDeployment",
        "codedeploy:PutLifecycleEventHookExecutionStatus",
        "codepipeline:GetPipelineState"
      ],
      "Resource": "*"
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject"
      ],
      "Resource": [
        "arn:aws:s3:::${codepipeline-bucket}/pipelineExecutions/*"
      ]
    }
  ]
}
