resource "random_string" "random" {
  length  = 12
  special = false
  upper   = false
  number  = false
}

resource "aws_s3_bucket" "codepipeline-artifacts" {
  acl           = "private"
  bucket        = "codepipeline-artifacts-${random_string.random.result}"
  force_destroy = true
  lifecycle_rule {
    prefix  = ""
    enabled = true
    noncurrent_version_expiration {
      days = 30
    }
  }
  lifecycle_rule {
    prefix  = "pipelineExecutions/"
    enabled = true
    expiration {
      days = 1
    }
  }
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "aws:kms"
      }
    }
  }
  tags = merge(local.common-tags,
    {
      Name = "codepipeline-artifacts-${random_string.random.result}"
    }
  )
  versioning {
    enabled = true
  }
}
