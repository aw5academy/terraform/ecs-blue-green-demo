data "external" "ifconfig" {
  program = ["bash", "${path.module}/ifconfig.sh"]
}

################################################
# ALB
################################################
resource "aws_security_group" "lb" {
  description = "${var.app}-lb"
  name        = "${var.app}-lb"
  tags        = merge(local.common-tags,
    {
      Name = "${var.app}-lb"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_security_group_rule" "lb-http-80" {
  cidr_blocks       = [data.external.ifconfig.result["public_ip"]]
  description       = "HTTP access"
  from_port         = 80
  protocol          = "tcp"
  security_group_id = aws_security_group.lb.id
  to_port           = 80
  type              = "ingress"
}

resource "aws_security_group_rule" "lb-http-test" {
  cidr_blocks       = ["${aws_eip.nat-a.public_ip}/32", "${aws_eip.nat-b.public_ip}/32"]
  description       = "HTTP access"
  from_port         = var.test-listener-port
  protocol          = "tcp"
  security_group_id = aws_security_group.lb.id
  to_port           = var.test-listener-port
  type              = "ingress"
}

resource "aws_security_group_rule" "lb-http-to-ecs" {
  description              = "HTTP access to ECS"
  from_port                = var.container-port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.lb.id
  source_security_group_id = aws_security_group.ecs.id
  to_port                  = var.container-port
  type                     = "egress"
}

################################################
# ECS
################################################
resource "aws_security_group" "ecs" {
  description = "${var.app}-ecs"
  name        = "${var.app}-ecs"
  tags        = merge(local.common-tags,
    {
      Name = "${var.app}-ecs"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_security_group_rule" "ecs-http-from-lb" {
  description              = "HTTP access from ALB"
  from_port                = var.container-port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.ecs.id
  source_security_group_id = aws_security_group.lb.id
  to_port                  = var.container-port
  type                     = "ingress"
}

resource "aws_security_group_rule" "ecs-outbound" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.ecs.id
  to_port           = 0
  type              = "egress"
}

################################################
# CodeBuild
################################################
resource "aws_security_group" "codebuild" {
  description = "${var.app}-codebuild"
  name        = "${var.app}-codebuild"
  tags        = merge(local.common-tags,
    {
      Name = "${var.app}-codebuild"
    }
  )
  vpc_id = aws_vpc.main.id
}

resource "aws_security_group_rule" "codebuild-egress" {
  cidr_blocks       = ["0.0.0.0/0"]
  description       = "Allowing full outbound access"
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.codebuild.id
  to_port           = 0
  type              = "egress"
}
