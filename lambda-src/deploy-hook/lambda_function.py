import json
import boto3
import os

AWS_REGION = os.getenv('AWS_REGION')
BUCKET_NAME = os.getenv('BUCKET_NAME')

CODE_DEPLOY = boto3.client('codedeploy', region_name=AWS_REGION)
CODE_PIPELINE = boto3.client('codepipeline', region_name=AWS_REGION)
S3 = boto3.client('s3', region_name=AWS_REGION)

def lambda_handler(event, context):

  deploymentId = event['DeploymentId']
  lifecycleEventHookExecutionId = event['LifecycleEventHookExecutionId']
  
  try:
    applicationName = CODE_DEPLOY.get_deployment(
      deploymentId=deploymentId
    )['deploymentInfo']['applicationName']
    
    pipelineState = CODE_PIPELINE.get_pipeline_state(
      name=applicationName
    )
    
    for stageState in pipelineState['stageStates']:
      if stageState['stageName'] != 'Deploy':
        continue
      for actionState in stageState['actionStates']:
        if actionState['actionName'] != 'Deploy':
          continue
        if actionState['latestExecution']['externalExecutionId'] == deploymentId:
          pipelineExecutionId = stageState['latestExecution']['pipelineExecutionId']

    fileName = f"/tmp/{pipelineExecutionId}"
    file = open(fileName, "w")
    file.write("export DEPLOYMENT_ID=" + deploymentId + "\n")
    file.write("export HOOK_EXECUTION_ID=" + lifecycleEventHookExecutionId + "\n")
    file.close()

    objectKey = f"pipelineExecutions/{applicationName}/{pipelineExecutionId}"
    S3.upload_file(fileName, BUCKET_NAME, objectKey)
    
  except Exception as e:
    CODE_DEPLOY.put_lifecycle_event_hook_execution_status(
      deploymentId=deploymentId,
      lifecycleEventHookExecutionId=lifecycleEventHookExecutionId,
      status='Failed'
    )
