resource "aws_ecs_cluster" "app" {
  name = var.app
  tags = merge(local.common-tags,
    {
      Name = var.app
    }
  )
}

# Define a placeholder task definition just so we can launch the ECS service with Terraform.
# Our CodeDeploy deployments will create the actual task definition.
resource "aws_ecs_task_definition" "app" {
  family                   = var.app
  container_definitions    = templatefile("${path.module}/templates/container_definitions.json.tpl", {})
  cpu                      = "256"
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  memory                   = "512"
  tags                     = merge(local.common-tags,
    {
      Name = var.app
    }
  )
}

resource "aws_ecs_service" "app" {
  name                              = var.app
  cluster                           = aws_ecs_cluster.app.id
  task_definition                   = aws_ecs_task_definition.app.arn
  desired_count                     = "1"
  depends_on                        = [aws_lb.app]
  enable_ecs_managed_tags           = "true"
  health_check_grace_period_seconds = "60"
  launch_type                       = "FARGATE"
  propagate_tags                    = "SERVICE"
  platform_version                  = "1.4.0"
  deployment_controller {
    type = "CODE_DEPLOY"
  }
  lifecycle {
    ignore_changes = [
      task_definition,
      load_balancer
    ]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.blue.arn
    container_name   = "main"
    container_port   = var.container-port
  }
  network_configuration {
    security_groups  = [aws_security_group.ecs.id]
    subnets          = [aws_subnet.private-a.id, aws_subnet.private-b.id]
  }
  tags = merge(local.common-tags,
    {
      Name = var.app
    }
  )
}
