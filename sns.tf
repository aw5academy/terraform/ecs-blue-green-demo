resource "aws_sns_topic" "alarm" {
  name              = "${var.app}-alarm"
  tags = merge(local.common-tags,
    {
      Name = "${var.app}-alarm"
    }
  )
}
