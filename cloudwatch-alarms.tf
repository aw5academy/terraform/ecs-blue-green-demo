resource "aws_cloudwatch_metric_alarm" "ecs-404s" {
  alarm_actions       = [aws_sns_topic.alarm.arn]
  alarm_name          = "${var.app}-ecs-404s"
  alarm_description   = "Alarms if there are 404 status codes in the ECS application logs."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = local.cw-ecs-404-metric-name
  namespace           = local.cw-ecs-namespace
  ok_actions          = [aws_sns_topic.alarm.arn]
  period              = "60"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}
