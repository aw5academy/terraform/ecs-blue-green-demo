resource "aws_codecommit_repository" "app" {
  repository_name = var.app
  tags = merge(local.common-tags,
    {
      Name = var.app
    }
  )
}
